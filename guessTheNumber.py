# template for "Guess the number" mini-project
# input will come from buttons and an input field
# all output for the game will be printed in the console
import random
import simplegui

# initialize global variables used in your code
secret_Number = 0
guesses = 7
range = 100

# helper function to start and restart the game
def new_game():
    global secret_Number, guesses
    
    if range == 100:
        guesses = 7
    else:
        guesses = 10
        
    secret_Number = random.randrange(0, range - 1)
    
    print ""
    print "New game starting! Range is between between 0 and", range
    print "You have",guesses,"guesses"
    
# define event handlers for control panel

def range100():
    # button that changes range to range [0,100) and restarts
    global range, guesses
    range = 100
    new_game()

def range1000():
    # button that changes range to range [0,1000) and restarts
    global range, guesses
    range = 1000
    new_game()
    
def input_guess(guess):
    # main game logic goes here
    global guesses
    guess = int(guess)
    
    print "\nGuess is", guess
        
    if guess == secret_Number:
        print "Correct guess!"
        new_game()
    elif guesses == 1:
        print "You ran out of guesses. The correct number is",secret_Number
        new_game()
    elif guess > secret_Number:
        guesses -= 1        
        print "Lower"
        print "Your remaining guesses are", guesses
    else:
        guesses -= 1
        print "Higher"
        print "Your remaining guesses are", guesses
    
# create frame
frame = simplegui.create_frame("Guess the Number!", 200, 200)

# register event handlers for control elements
frame.add_button("Range: 0 - 100", range100)
frame.add_button("Range: 0 - 1000", range1000)

# call new_game and start frame
new_game()
frame.start()
frame.add_input("Enter your guess", input_guess, 100)

# always remember to check your completed program against the grading rubric